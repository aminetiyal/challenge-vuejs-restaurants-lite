# challenge

## Install
```bash
# install dependencies
$ npm install
```

## For production
```bash
# build for production and launch server
$ npm run build
$ npm run start
```

## For development
```bash
# serve with hot reload at localhost:3000
$ npm run dev
```
