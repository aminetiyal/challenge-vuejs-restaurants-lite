import { mount } from '@vue/test-utils'
import Index from '@/pages/index.vue'
import Meals from '@/pages/meals.vue'
import Restaurants from '@/pages/restaurants.vue'

describe('Index', () => {
  test('Index page can be rendered', () => {
    const wrapper = mount(Index)
    expect(wrapper.vm).toBeTruthy()
  })
})

describe('Meals', () => {
  test('Meals page can be rendered', () => {
    const wrapper = mount(Meals)
    expect(wrapper.vm).toBeTruthy()
  })
})

describe('Restaurants', () => {
  test('Restaurants page can be rendered', () => {
    const wrapper = mount(Restaurants)
    expect(wrapper.vm).toBeTruthy()
  })
})
