const defaultTheme = require('tailwindcss/defaultTheme')

module.exports = {
  jit: true,
  darkMode: 'class',
  theme: {
    extend: {
      fontFamily: {
        sans: ['Inter var', ...defaultTheme.fontFamily.sans],
      },
    },
  },
  plugins: [],
}
